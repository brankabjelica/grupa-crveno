import React from 'react';
import { useNavigate } from 'react-router-dom';
import { LocalizationContext } from '../context/LanguageContext';
import "./HomePage.css";
import "@fontsource/roboto";
import "@fontsource/open-sans";
import NavigatorAuth from "../infrastructure/components/NavigatorAuth";



const HomePageAuth = () => {
    const { t } = React.useContext(LocalizationContext);
    let navigate = useNavigate()

    const navigateSignIn = () => {
        navigate("/signin")
        navigate(0)
    }

    const navigateSignUp = () => {
        navigate("/signup")
        navigate(0)
    }


    return (
        <div className="background-custom">
            <div className="container">
                <NavigatorAuth />
                <div className="central-page">
                    <div>
                        <h2 className="welcome_1">{t('welcome_1')}</h2>                    
                        <h2 className="welcome_2">{t('welcome_2')}</h2>
                        <h2 className="welcome_3">{t('welcome_3')}</h2>
                    </div>
                    <div className="welcomeContent">
                        <p className="welcomeContent_1">{t('welcomeContent_1')}</p>
                        <p className="welcomeContent_2">{t('welcomeContent_2')}</p>
                    </div>
                    <div className="buttons">
                        <button className="signIn-button ripple" onClick={navigateSignIn}>{t("signIn")}</button>
                        <p className="separator circle">{t('or')}</p>
                        <button className="signUp-button ripple" onClick={navigateSignUp}>{t("signUp")}</button>
                    </div>
                    
                </div>
            </div>
        </div>
    );
}

export default HomePageAuth;
