import React from 'react';
import { useNavigate } from 'react-router-dom';
import { LocalizationContext } from '../../context/LanguageContext';
import './SignIn.css';
import NavigatorAuth from "./../../infrastructure/components/NavigatorAuth";

import * as yup from 'yup';
import { useFormik } from 'formik';

import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';


// Password Visibily Eye-icon ON/OFF

import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';

// Backend API

import client_auth from '../../apis/client_auth';


function Copyright(props) {
  const { t } = React.useContext(LocalizationContext);
  return (
    <Typography className="copyright" variant="body2" color="#ffff" align="center" {...props}>
      {t("copyright")}
      <Link color="inherit" href="https://mui.com/">
        {t("copyright2")}
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}



export default function SignIn() {
  const { t } = React.useContext(LocalizationContext);

  const navigate = useNavigate()

  const [values, setValues] = React.useState({
    username: '',
    password: '',
    showPassword: false,
  });

  /*const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };*/

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const [error, setError] = React.useState('')

  const onSignIn = async (values) => {
    try {
      const user = {
        username: values.username,
        password: values.password,
      };

      const response = await client_auth.post('/auth/jwt/create/', user);

      const tok = JSON.stringify(response.data);
      const parsedData = JSON.parse(tok);

      localStorage.setItem('token', parsedData.access);
      localStorage.setItem('refreshToken', parsedData.refresh);


      if (values.remember === true) {
        localStorage.setItem('username', values.username);
        localStorage.setItem('password', values.password);
      }

      navigate('/');
      navigate(0);


    } catch (error) {
      let err = Object.values(error.response.data)
      console.log(error)
      if (err[0] === "No active account found with the given credentials.") {
        setError("Ovaj korisnik ne postoji.")
      }
      else {
        setError('Error')
      }
      return

    }
  }

  const validationSchema = yup.object({
    username: yup.string()
      .required('Obavezno polje'),
    password: yup.string()
      .required('Obavezno polje')
      .min(8, 'Minimum 8 karaktera')
      .max(40, 'Maksimum 40 karaktera'),

  });

  const formik = useFormik({
    initialValues: values,
    validationSchema: validationSchema,
    onSubmit: onSignIn,
  });


  return (
    <div className="background-custom">
      <div className="container">
        <NavigatorAuth />
        
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <Box
            sx={{
              marginTop: 8,
              display: 'flex',
              flexDirection: 'column',
              alignItems: "center",
              bgcolor: '#ffff',
              padding: 2.5,
              boxShadow: 8,
            }}
          >
            <Avatar className='avatar' sx={{ m: 1, bgcolor: 'error.main' }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              {t("signIn")}
            </Typography>
            <Box className="username" component="form" onSubmit={formik.handleSubmit} noValidate sx={{ mt: 1 }}>
              <TextField
                color="error"
                margin="normal"
                required
                fullWidth
                id="username"
                label={t("username")}
                value={formik.values.username}
                onChange={formik.handleChange}
                error={formik.touched.username && Boolean(formik.errors.username)}
                helperText={formik.touched.username && formik.errors.username}
                name="username"
                autoComplete="username"
              />
              <TextField
                color="error"
                margin="normal"
                required
                fullWidth
                name="password"
                label={t("password")}
                type={values.showPassword ? 'text' : 'password'}
                //value={values.password}
                //onChange={handleChange('password')}
                value={formik.values.password}
                onChange={formik.handleChange}
                error={formik.touched.password && Boolean(formik.errors.password)}
                helperText={formik.touched.password && formik.errors.password}
                id="password"
                autoComplete="current-password"

                InputProps={{
                  endAdornment: <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {values.showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }}
              />
              <FormControlLabel
                control={<Checkbox value="remember" color="error" />}
                label={t("rememberSI")}
              />
              <Button
                color="error"
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                {t("buttonSignIn")}
              </Button>
              {error !== '' && <p style={{color: 'red'}}>{error}</p>}
              <Grid container>
                <Grid item xs>
                  <Link href="#" variant="body2" color="error.main">
                    {t("forgotPasswordSI")}
                  </Link>
                </Grid>
                <Grid item>
                  <Link href="/signup" className="signUp" variant="body2" color="error.main"
                  // onClick={navigateSignUp}
                  >
                    {t("questionAccountSI")}
                  </Link>
                </Grid>
              </Grid>
            </Box>
          </Box>
          <Copyright sx={{ mt: 6.5, mb: 5.5 }} />
        </Container>
      </div>
    </div>
  );
}


