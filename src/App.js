import React, { useRef } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import I18n from 'i18n-js';
// import Navigation from "./pages/Navigation";
// import LanguageDropdown from './infrastructure/components/LanguageDropdown';
import { LocalizationContext } from './context/LanguageContext';

import { createTheme, ThemeProvider } from '@mui/material/styles';

import AppStore from './pages/AppStore';
import HomePage from './pages/HomePage';
import HomePageAuth from './pages/HomePageAuth';
import SignIn from './pages/Auth/SignIn';
import SignUp from './pages/Auth/SignUp';
import Navigator from './infrastructure/components/Navigator';
import PageNotFound from './pages/PageNotFound';
import ProfilePage from './pages/ProfilePage';


const App = () => {
  const [locale, setLocale] = React.useState('cyr');
  var isActive = useRef(true);

  const theme = createTheme();


  const localizationContext = React.useMemo(
    () => ({
      t: (scope, options) => I18n.t(scope, { locale, ...options }),
      locale,
      setLocale,
    }),
    [locale]
  );

  React.useEffect(() => {
    let language = localStorage.getItem('language');
    if (isActive.current === true) setLocale(language || 'cyr');
    return () => {
      isActive.current = false;
    };
  }, []);


  let routes;


  if (localStorage.getItem('refreshToken') === null) {
    routes = (
      <>
        <Routes>
          <Route path='/' element={<HomePageAuth />} />
          <Route path='/signin' element={<SignIn />} />
          <Route path='/signup' element={<SignUp />} />
          <Route path='*' element={<PageNotFound />} />
        </Routes>
      </>
    );
  } else {
    routes = (
      <>
        <Navigator />
        <Routes>
          <Route path='/' element={<HomePage />} />
          <Route path='/profile' element={<ProfilePage />} />
          <Route path='*' element={<PageNotFound />} />
        </Routes>
      </>
    );
  }


  return (
    <div>
      <ThemeProvider theme={theme} >
      <Router>
      <LocalizationContext.Provider value={localizationContext}>
        <AppStore>
          <main>{routes}</main>
        </AppStore>
        </LocalizationContext.Provider>
      </Router>
      </ThemeProvider>
    </div>
  );
};

export default App;
