import * as React from 'react';
import './NavigatorAuth.css';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import MenuItem from '@mui/material/MenuItem';
import Stack from "@mui/material/Stack";

import LanguageDropdown from "./LanguageDropdown";
import { LocalizationContext } from '../../context/LanguageContext';
import "@fontsource/roboto";
import "@fontsource/open-sans";
import { Grow, Link } from '@mui/material';


const pages = ['HomePage'];
// const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];

const NavigatorAuth = () => {
  const [anchorElNav, setAnchorElNav] = React.useState(null);

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  
  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const { t } = React.useContext(LocalizationContext);

  return (
    <AppBar className="app-bar-1" position="static" sx={{ bgcolor: 'error.main'}}>
      <Container maxWidth="xl">
        <Toolbar disableGutters height="max-content" className="toolbar-1">
          
          <Box>
            <Link 
              className="logo" 
              href="/"
              variant="h6"
              noWrap
              component="a"
              sx={{
                mr: 1,
                display: { xs: 'none', md: 'flex' },
              }}
            >
              {/* <img src="friendly_logo.png" alt="logo" /> */}
              <img src="https://imgur.com/zUhfWXf.png" alt="logo" to="/" />
            </Link>
          </Box>

          <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>

            <Menu
              /* href="/" */
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: 'block', md: 'none' },
              }}
            >
              {pages.map((page) => (
                <MenuItem key={page} onClick={handleCloseNavMenu}>
                  <Typography 
                    noWrap
                    component="a"
                    href="/"
                    sx={{
                      flexGrow: 1, 
                      textAlign: "center", 
                      alignItems: "center", 
                      display: 'flex', 
                      justifyContent: "center" 
                    }}
                    >{t('homePage')}
                  </Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
          <Box>
            <Link 
              className="logo" 
              href="/"
              variant="h6"
              noWrap
              component="a"
              sx={{
                horizontal: 'center',
                mr: 2,
                display: { xs: 'flex', md: 'none' },
                flexGrow : 1,
                display: 'flex', 
                textAlign: "center", 
                alignItems: "center", 
                justifyContent: "center" 
              }}
            >
              {/* <img src="images/friendly_logo.png" alt="logo" /> */}
              <img src="https://imgur.com/zUhfWXf.png" alt="logo" to="/" />
            </Link>
          </Box>
          <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
            {pages.map((page) => (
              <Typography
                className="home"
                noWrap
                component="a"
                href="/"
                key={page}
                onClick={handleCloseNavMenu}
                sx={{ 
                  flexGrow: 1, 
                  textAlign: "center", 
                  alignItems: "center", 
                  display: 'flex', 
                  justifyContent: "center" 
                }}
              >
                {t('homePage')}
              </Typography>
            ))}
          </Box>

          <Stack direction={"row"} spacing={2} className='language-dropdown'>
            <LanguageDropdown />
          </Stack>

        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default NavigatorAuth;
