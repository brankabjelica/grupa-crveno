import React from 'react';
// import { useNavigate } from 'react-router-dom';

import './LanguageDropdown.css';
import "@fontsource/roboto";
import "@fontsource/open-sans";
// import Icon from '@mui/icons-material/Language';
// import 'flag-icons';

import Icon from '@mui/material/Button';
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import { IconButton } from '@mui/material';


const LanguageDropdown = () => {
  console.disableYellowWarnin = true;
  const [locale, setLocale] = React.useState(localStorage.getItem('language'));

  const handleSetLocale = (value) => {
    setLocale(value);
    localStorage.setItem('language', value);
    window.location.reload()
  };

  return (
    <>
      <div className='bg_top_nav'>
        <IconButton
          sx={{
            "&:hover": {
              backgroundColor: "transparent",
              cursor: "default"
              }
          }}
          disableRipple={true}
          onClick={(event) => handleSetLocale(event.target.value)}
          >
          <Icon
            startIcon={<Avatar src= {'srpski-jezik.png'} variant = "square" sx = {{height: "auto"}} />}
            className= 'flag'
          >
          </Icon>
          <Button
            className='button_lang'
            variant='button_lang'
            value='sr'
          >
            lat
          </Button>
        </IconButton>
        <IconButton
          sx={{
            "&:hover": {
              backgroundColor: "transparent",
              cursor: "default"
              }
          }}
          disableRipple={true}
          onClick={(event) => handleSetLocale(event.target.value)}
          >
          <Icon
            startIcon={<Avatar src= {'srpski-jezik.png'} variant = "square" sx = {{height: "auto"}} />}
            className= 'flag'
          >
          </Icon>
          <Button
            className='button_lang'
            variant='button_lang'
            value='cyr'
          >
            ћир
          </Button>
        </IconButton>
        <IconButton
          sx={{
            "&:hover": {
              backgroundColor: "transparent",
              cursor: "default"
              }
          }}
          disableRipple={true}
          onClick={(event) => handleSetLocale(event.target.value)}
          >
          <Icon
            startIcon={<Avatar src= {'engleski-jezik.png'} variant = "square" sx = {{height: "auto"}} />}
            className= 'flag'
          >
          </Icon>
          <Button
            className='button_lang'
            variant='button_lang'
            value='en'
          >
            eng
          </Button>
        </IconButton>
      </div>
    </>
  );
};

export default LanguageDropdown;