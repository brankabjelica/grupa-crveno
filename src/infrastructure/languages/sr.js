const sr = {
    homePage: 'Početna strana',
    welcome_1: 'Dobrodošli',
    welcome_2: 'Na najzabavniju i najdruželjubiviju društvenu mrežu',
    welcome_3: 'ikada uploadovanu na GitLab !!!',
    welcomeContent_1: 'Povezujemo vas sa porodicom, prijateljima, poslovnim partnerima',
    welcomeContent_2: 'i FRIENDLY ljudima širom sveta! Udaljenost više nije prepreka!',
    submit: 'Potvrdi',
    signIn: 'Uloguj se',
    signUp: 'Registruj se',
    or: 'ili',
    username: 'Korisničko ime',
    password: 'Šifra',
    confirmPassword: 'Potvrdi šifru',
    rememberSI: 'Zapamti me',
    buttonSignIn: 'Uloguj se',
    forgotPasswordSI: 'Zaboravljena šifra?',
    passwordChange: 'Uspešno ste promenili šifru..',
    passwordSimilar: 'Lozinka je slična imejlu.',
    passwordTooCommon: 'Lozinka je uobičajena.',
    invalidPassword: 'Neispravna lozinka.',
    check: 'Proverite da li ste dobro uneli podatke.',
    dontMatch: 'Lozinke se ne podudaraju',
    questionAccountSI: 'Nemate nalog? Prijavite se.',
    questionAccountSU: 'Već imate nalog? Ulogujte se.',
    copyright: 'Autorska prava © ',
    copyright2: 'Povezani veb sajt',
    nameSurname: 'Ime i prezime',
    emailAddress: 'Email adresa',
    notifications: 'Želim dobiti inspiraciju, marketinške promocije i ažuriranja putem e-pošte.',
    required: 'Obavezno polje',
    date_of_birth: 'Datum rođenja: ',
    city: 'Grad: ',
    address: 'Adresa: ',
    phone: 'Telefon: ',
    successfullyCreate: 'Uspešno ste kreirali profil.',
    successfullyEdit: 'Uspešno ste izmenili profil.',
    createProfile: 'KREIRAJ PROFIL',
    changeProfile: 'IZMENI PROFIL',
    changePassword: 'IZMENI LOZINKU',
    current_password: 'Trenutna lozinka',
    new_password: 'Nova lozinka',
    re_new_password: 'Ponovljena nova lozinka',
    logo: 'LOGO',
    profile:'PROFIL',
    logout: 'Odjavi se',
    deleteAccount: 'OBRIŠI NALOG',
    pageNotFound: 'Tražena stranica nije pronađena.',
    hello: 'Zdravo, ',

};

export default sr;
